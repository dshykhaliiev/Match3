'use strict';

export default class JSONLoader {
    constructor() {

    }

    load(filename) {
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function(){
            if (xhr.readyState === 4 && xhr.status === 200){
                let jsonObj = JSON.parse(xhr.responseText);
                this.onLoaded(jsonObj);
            }
        }.bind(this);
        xhr.open('GET', filename, true);
        xhr.send();
    }

    onLoaded(callback) {
        this.onLoaded = callback;
    }
}