'use strict';

export default class OSHelper {
    constructor() {
        let browser,
            version,
            mobile,
            os,
            osversion,
            bit,
            ua = window.navigator.userAgent,
            platform = window.navigator.platform;

        this.os = os;

        //Internet Explorer
        if ( /MSIE/.test(ua) ) {

            browser = 'Internet Explorer';

            if ( /IEMobile/.test(ua) ) {
                mobile = 1;
            }

            version = /MSIE \d+[.]\d+/.exec(ua)[0].split(' ')[1];

            //Google Chrome
        } else if ( /Chrome/.test(ua) ) {

            //Chromebooks
            if ( /CrOS/.test(ua) ) {
                platform = 'CrOS';
            }
            browser = 'Chrome';
            version = /Chrome\/[\d\.]+/.exec(ua)[0].split('/')[1];

            // Opera Browser
        } else if ( /Opera/.test(ua) ) {

            browser = 'Opera';

            if ( /mini/.test(ua) || /Mobile/.test(ua) ) {
                mobile = 1;
            }

            //Android Browser
        } else if ( /Android/.test(ua) ) {

            browser = 'Android Webkit Browser';
            mobile = 1;
            this.os = /Android\s[\.\d]+/.exec(ua)[0];

            //Mozilla firefox
        } else if ( /Firefox/.test(ua) ) {

            browser = 'Firefox';

            if ( /Fennec/.test(ua) ) {
                mobile = 1;
            }
            version = /Firefox\/[\.\d]+/.exec(ua)[0].split('/')[1];

            //Safari
        } else if ( /Safari/.test(ua) ) {

            browser = 'Safari';

            if ( (/iPhone/.test(ua)) || (/iPad/.test(ua)) || (/iPod/.test(ua)) ) {
                this.os = 'iOS';
                mobile = 1;
            }

        }
        if ( !version ) {

            version = /Version\/[\.\d]+/.exec(ua);

            if (version) {
                version = version[0].split('/')[1];
            } else {
                version = /Opera\/[\.\d]+/.exec(ua)[0].split('/')[1];
            }

        }

        if ( platform === 'MacIntel' || platform === 'MacPPC' ) {
            this.os = 'Mac OS X';
            osversion = /10[\.\_\d]+/.exec(ua)[0];
            if ( /[\_]/.test(osversion) ) {
                osversion = osversion.split('_').join('.');
            }
        } else if ( platform === 'CrOS' ) {
            this.os = 'ChromeOS';
        } else if ( platform === 'Win32' || platform == 'Win64' ) {
            this.os = 'Windows';
            bit = platform.replace(/[^0-9]+/,'');
        } else if ( !this.os && /Android/.test(ua) ) {
            this.os = 'Android';
        } else if ( !this.os && /Linux/.test(platform) ) {
            this.os = 'Linux';
        } else if ( !this.os && /Windows/.test(ua) ) {
            this.os = 'Windows';
        }
        window.ui = {
            browser : browser,
            version : version,
            mobile : mobile,
            os : this.os,
            osversion : osversion,
            bit: bit
        };


    }




}