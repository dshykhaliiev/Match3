'use strict';

import GameModel from "../model/game_model";
import * as PIXI from "pixi.js";

export default class Resizer {
    constructor(view) {
        this.view = view;

        this.needShowTutorial = true;

        this.renderer = PIXI.autoDetectRenderer(GameModel.SCREEN_WIDTH, GameModel.SCREEN_HEIGHT, {view: this.view, transparent: true});

        window.addEventListener('resize', this.fitRendererToWindow.bind(this));

        this.fitRendererToWindow();
    }

    fitRendererToWindow() {
        let windowWidth = window.innerWidth > GameModel.SCREEN_WIDTH ? GameModel.SCREEN_WIDTH : window.innerWidth;
        let windowHeight = window.innerHeight > GameModel.SCREEN_HEIGHT ? GameModel.SCREEN_HEIGHT : window.innerHeight;

        if (windowWidth > windowHeight) {
            windowWidth = windowHeight * GameModel.ASPECT_RATIO;
        } else {
            windowHeight = windowWidth / GameModel.ASPECT_RATIO;
        }

        this.renderer.view.style.marginLeft = (window.innerWidth - windowWidth) / 2 + 'px';
        this.renderer.view.style.width = windowWidth + 'px';
        this.renderer.view.style.height = windowHeight + 'px';

        let scale = windowWidth / GameModel.SCREEN_WIDTH;
        let el = document.getElementById("main");
        el.style.backgroundSize=1280 * scale + 'px ' + 1080 * scale + 'px';

        if (this.needShowTutorial) {
            this.enableTutorial();
        } else {
            this.disableTutorial();
        }
    }

    enableTutorial() {
        let el = document.getElementById("main");
        el.style.background = 'linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), ' +
            'url(assets/game_bg.png) ' +
            'no-repeat ' +
            'center ' +
            'center';
    }

    disableTutorial() {
        let el = document.getElementById("main");
        el.style.background = 'url(assets/game_bg.png) ' +
            'no-repeat ' +
            'center ' +
            'center';
    }

    set needShowTut(value) {
        this.needShowTutorial = value;
    }
}