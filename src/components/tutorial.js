'use strict';

import GameModel from "../model/game_model";
import * as PIXI from "pixi.js";

export default class Tutorial extends PIXI.Container {
    constructor(map) {
        super();

        this.map = map;

        this.addTint();

        this.createText();
    }

    addTint() {
        for (let rowIndex = 0; rowIndex < this.map.length; rowIndex++) {
            let row = this.map[rowIndex];

            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                let point = this.map[rowIndex][colIndex];

                if (!point.isTutorial) {
                    this.createCellTint(point);
                }
            }
        }
    }

    createCellTint(point) {
        let tint = new PIXI.Graphics();
        tint.beginFill(0x000000);
        tint.alpha = 0.7;
        tint.drawRect(point.x, point.y, GameModel.CELL_SIZE, GameModel.CELL_SIZE);
        tint.interactive = true;
        tint.on('pointerdown', this.onClick.bind(this));
        this.addChild(tint);
    }

    onClick(event) {
        event.stopPropagation();
    }

    createText() {
        const style = new PIXI.TextStyle({
            fontFamily: 'Arial',
            fontSize: 20,
            fontWeight: 'bold',
            fill: ['#ffffff'],
            dropShadow: true,
            dropShadowColor: '#000000',
            dropShadowBlur: 1,
            dropShadowDistance: 3,
        });

        let tutorialText = new PIXI.Text("Make a swipe to get three items in a row", style);
        tutorialText.anchor.x = 0.5;
        tutorialText.x = GameModel.SCREEN_WIDTH / 2;
        tutorialText.y = GameModel.SCREEN_HEIGHT - tutorialText.height - 10;
        this.addChild(tutorialText);
    }
}