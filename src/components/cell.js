'use strict';

import * as PIXI from 'pixi.js';
import TweenMax from "gsap";
import GameModel from "../model/game_model";
import GameView from "../view/game_view";

export default class Cell extends PIXI.Container {
    constructor(item) {
        super();

        this.item = item;

        this.init();
    }

    init() {
        let texture = PIXI.utils.TextureCache[`item_${this.item}.png`];
        let itemImg = new PIXI.Sprite(texture);
        itemImg.width = GameModel.CELL_SIZE;
        itemImg.height = GameModel.CELL_SIZE;
        this.addChild(itemImg);
        this.itemImg = itemImg;

    }

    set col(value) {
        this._col = value;
    }

    get col() {
        return this._col;
    }

    set row(value) {
        this._row = value;
    }

    get row() {
        return this._row;
    }

    addItem(item) {
        this.item = item;

        let texture = PIXI.utils.TextureCache[`item_${this.item}.png`];
        let itemImg = new PIXI.Sprite(texture);
        itemImg.width = GameModel.CELL_SIZE;
        itemImg.height = GameModel.CELL_SIZE;
        this.addChild(itemImg);
        this.itemImg = itemImg;
    }

    removeItem(point, skipAnimation) {
        let skip = skipAnimation || false;

        this.item = -1;

        if (skip) {
            this.removeChild(this.itemImg);
            this.itemImg = null;
            return;
        }

        TweenMax.to(this, 0.1 + GameView.DEBUG_TIME, {alpha: 0, onComplete: function() {
            this.alpha = 1;

            this.removeChild(this.itemImg);
            this.itemImg = null;

            this.onRemoveComplete(this, point);

        }.bind(this) });


    }

    onRemoveComplete(callback) {
        this.onRemoveComplete = callback;
    }

}