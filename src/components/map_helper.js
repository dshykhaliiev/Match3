'use strict';

import GameModel from "../model/game_model";

export default class MapHelper {
    constructor(gameModel) {
        this.gameModel = gameModel;
    }

    generate() {
        let map = [];

        for (let row = 0; row < this.gameModel.FIELD_HEIGHT; row++) {
            let line = [];
            map.push(line);

            for (let col = 0; col < this.gameModel.FIELD_WIDTH; col++) {
                let point = {};
                point.x = GameModel.FIELD_LEFT_PADDING + col * GameModel.CELL_SIZE;
                point.y = GameModel.FIELD_TOP_PADDING + row * GameModel.CELL_SIZE;

                point.item = this.getRandomItem();

                // make sure we don't have matches on startup
                while (col >= 2 && map[row][col - 1].item === point.item && map[row][col - 2].item === point.item ||
                     row >= 2 && map[row - 1][col].item === point.item && map[row - 2][col].item === point.item) {
                    point.item = this.getRandomItem();
                }

                point.col = col;
                point.row = row;

                line.push(point);
            }
        }

        this.addPossibleMatch(map);

        return map;
    }

    getRandomItem() {
        return Math.round(Math.random() * 5);
    }

    isVerticalDirection(map) {
        let isVertical = Math.random() > 0.5;

        if (map.length < 3) {
            isVertical = false;
        }

        if (map[0].length < 3) {
            isVertical = true;
        }

        return isVertical;
    }

    addPossibleMatch(map) {
        // a hack to guarantee we have at least one possible match
        if (this.isVerticalDirection(map)) {
            let row = Math.round(Math.random() * (map.length - 5));
            let col = Math.round(Math.random() * (map[0].length - 2));
            let point = map[row][col];
            point.isTutorial = true;

            map[row + 1][col].item = point.item;
            map[row + 1][col].isTutorial = true;
            map[row + 2][col + 1].item = point.item;
            map[row + 2][col + 1].isTutorial = true;

            let item = this.getRandomItem();
            map[row + 2][col].isTutorial = true;
            while (map[row + 2][col].item === point.item) {
                item = this.getRandomItem();
                map[row + 2][col].item = item;
            }
        } else {
            let row = Math.round(Math.random() * (map.length - 2));
            let col = Math.round(Math.random() * (map[0].length - 5));
            let point = map[row][col];
            point.isTutorial = true;

            map[row][col + 1].item = point.item;
            map[row][col + 1].isTutorial = true;
            map[row + 1][col + 2].item = point.item;
            map[row + 1][col + 2].isTutorial = true;

            let item = this.getRandomItem();
            map[row][col + 2].isTutorial = true;

            while (map[row][col + 2].item === point.item) {
                item = this.getRandomItem();

                map[row][col + 2].item = item;

            }
        }
    }

    getAllItemsToRemove(map) {
        let itemsToRemove = [];

        for (let rowIndex = 0; rowIndex < map.length; rowIndex++) {
            let row = map[rowIndex];

            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                let point = map[rowIndex][colIndex];

                itemsToRemove = itemsToRemove.concat(this.getHorizMatches(point, map));
                itemsToRemove = itemsToRemove.concat(this.getVertMatches(point, map));
            }
        }

        return this.removeDuplicates(itemsToRemove);
    }

    getItemsToRemove(point, neighbour, map) {
        let itemsToRemove = [];

        itemsToRemove = itemsToRemove.concat(this.getHorizMatches(point, map));
        itemsToRemove = itemsToRemove.concat(this.getHorizMatches(neighbour, map));

        itemsToRemove = itemsToRemove.concat(this.getVertMatches(point, map));
        itemsToRemove = itemsToRemove.concat(this.getVertMatches(neighbour, map));


        return this.removeDuplicates(itemsToRemove);
    }

    removeDuplicates(itemsToRemove) {
        let uniqIds = {};

        let itemsFiltered = itemsToRemove.filter(function(item) {
            if (!uniqIds["" + item.row + item.col]) {
                uniqIds["" + item.row + item.col] = true;
                return true;
            }

            return false;
        });

        return itemsFiltered;
    }

    getHorizMatches(point, map) {
        let itemsToRemove = [];

        itemsToRemove = itemsToRemove.concat(this.getRightLine(point, map));
        itemsToRemove = itemsToRemove.concat(this.getLeftLine(point, map));
        itemsToRemove = itemsToRemove.concat(this.getMiddleLine(point, map));

        return itemsToRemove;
    }

    getRightLine(point, map) {
        let itemsToRemove = [];
        let itemsInARow = 1;
        let row = map[point.row];

        // check right
        for (let i = point.col + 1; i < row.length; i++) {
            let neighbour = row[i];

            if (neighbour.item === point.item && point.item !== -1) {
                itemsInARow++;
            } else {
                break;
            }
        }

        if (itemsInARow >= 3) {
            for (let j = 0; j < itemsInARow; j++) {
                itemsToRemove.push(row[point.col + j]);
            }
        }

        return itemsToRemove;
    }

    getLeftLine(point, map) {
        let itemsToRemove = [];
        let itemsInARow = 1;
        let row = map[point.row];
        // check left
        for (let i = point.col - 1; i > -1; i--) {
            let neighbour = row[i];

            if (neighbour.item === point.item && point.item !== -1) {
                itemsInARow++;
            } else {
                break;
            }
        }

        if (itemsInARow >= 3) {
            for (let j = 0; j < itemsInARow; j++) {
                itemsToRemove.push(row[point.col - j]);
            }
        }

        return itemsToRemove;
    }

    getMiddleLine(point, map) {
        // check middle
        let itemsToRemove = [];
        let row = map[point.row];
        let leftNeighbour = row[point.col - 1];
        let rightNeighbour = row[point.col + 1];

        if (leftNeighbour && rightNeighbour) {
            if (point.item === leftNeighbour.item && point.item === rightNeighbour.item  && point.item !== -1) {
                itemsToRemove = itemsToRemove.concat(leftNeighbour, point, rightNeighbour);
            }
        }

        return itemsToRemove;
    }

    getVertMatches(point, map) {
        let itemsToRemove = [];

        itemsToRemove = itemsToRemove.concat(this.getTopLine(point, map));
        itemsToRemove = itemsToRemove.concat(this.getBottomLine(point, map));
        itemsToRemove = itemsToRemove.concat(this.getVertMiddleLine(point, map));

        return itemsToRemove;
    }

    getBottomLine(point, map) {
        let itemsToRemove = [];
        let itemsInARow = 1;

        for (let i = point.row + 1; i < map.length; i++) {
            let neighbour = map[i][point.col];

            if (neighbour.item === point.item && point.item !== -1) {
                itemsInARow++;
            } else {
                break;
            }
        }

        if (itemsInARow >= 3) {
            for (let j = 0; j < itemsInARow; j++) {
                itemsToRemove.push(map[point.row + j][point.col]);
            }
        }

        return itemsToRemove;
    }

    getTopLine(point, map) {
        let itemsToRemove = [];
        let itemsInARow = 1;
        // let row = map[point.row];

        for (let i = point.row - 1; i > -1; i--) {
            let neighbour = map[i][point.col];

            if (neighbour.item === point.item && point.item !== -1) {
                itemsInARow++;
            } else {
                break;
            }
        }

        if (itemsInARow >= 3) {
            for (let j = 0; j < itemsInARow; j++) {
                itemsToRemove.push(map[point.row - j][point.col]);
            }
        }

        return itemsToRemove;
    }

    getVertMiddleLine(point, map) {
        let itemsToRemove = [];
        let topRow = map[point.row - 1];
        let bottomRow = map[point.row + 1];

        let topNeighbour;
        let bottomNeighbour;

        if (topRow && bottomRow) {
            topNeighbour = topRow[point.col];
            bottomNeighbour = bottomRow[point.col];
        }

        if (topNeighbour && bottomNeighbour) {
            if (point.item === topNeighbour.item && point.item === bottomNeighbour.item && point.item !== -1) {
                itemsToRemove = itemsToRemove.concat(topNeighbour, point, bottomNeighbour);
            }
        }

        return itemsToRemove;
    }

    fallDown(map) {
        let columns = [];

        for (let col = 0; col < this.gameModel.FIELD_WIDTH; col++) {

            let items = [];

            for (let row = map.length - 1; row > -1; row--) {
                let point = map[row][col];

                this.fallItem(point, items, row, col, map);
            }

            columns = columns.concat(items);
        }

        return columns;
    }

    fallItem(point, items, row, col, map) {
        if (point.item !== -1) {
            return;
        }

        for (let filledRow = row - 1; filledRow > - 1; filledRow--) {
            let filledPoint = map[filledRow][col];

            if (filledPoint.item === -1) {
                continue;
            }

            point.item = filledPoint.item;
            filledPoint.item = -1;

            let fallObject = {
                x: point.x,
                y: point.y,
                item: point.item,
                oldRow: filledPoint.row,
                col: point.col,
                row: row
            };

            items.push(fallObject);

            break;
        }
    }

    fillEmptyPoints(map) {
        let emptyPoints = [];

        for (let i = 0; i < map.length; i++) {
            let row = map[i];

            emptyPoints = emptyPoints.concat(row.filter(point => { return point.item === -1 }) );
        }

        emptyPoints.forEach(point => point.item = this.getRandomItem() );

        return emptyPoints;
    }
}