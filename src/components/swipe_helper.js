'use strict';

import MoveTypeEnum from "../enums/move_type_enum";

export default class SwipeHelper {
    constructor(rootContainer) {
        this.targets = [];

        rootContainer.interactive = true;
        rootContainer.on('pointerup', this.onRootPointerUp.bind(this));
        rootContainer.on('pointermove', this.onRootPointerMove.bind(this));
    }

    register(target, buttonMode) {

        target.buttonMode = buttonMode || false;
        target.interactive = true;
        target.on('pointerdown', this.onPointerDown.bind(this));
        target.on('pointerup', this.onPointerUp.bind(this));

        this.targets.push(target);
    }

    onPointerDown(event) {
        this.startX = event.data.global.x;
        this.startY = event.data.global.y;
        this.currentTarget = event.target;

        event.target.interactive = true;
        event.target.on('pointerdown', this.onPointerDown.bind(this));
        event.target.on('pointerup', this.onPointerUp.bind(this));
    }

    onPointerUp(event) {
        this.currentTarget = null;
    }

    onRootPointerUp(event) {
        this.currentTarget = null;
    }

    onRootPointerMove(event) {
        if (!this.currentTarget) {
            return;
        }

        let xShift = event.data.global.x - this.startX;
        let yShift = event.data.global.y - this.startY;

        if (xShift > 30 && Math.abs(yShift) < 10) {
            this.onSwipe(this.currentTarget, MoveTypeEnum.MOVE_RIGHT);
            this.currentTarget = null;
        } else if (xShift < -30 && Math.abs(yShift) < 10) {
            this.onSwipe(this.currentTarget, MoveTypeEnum.MOVE_LEFT);
            this.currentTarget = null;
        } else if (yShift > 30 && Math.abs(xShift) < 10) {
            this.onSwipe(this.currentTarget, MoveTypeEnum.MOVE_DOWN);
            this.currentTarget = null;
        } else if (yShift < -30 && Math.abs(xShift) < 10) {
            this.onSwipe(this.currentTarget, MoveTypeEnum.MOVE_UP);
            this.currentTarget = null;
        }
    }

    onSwipe(callback) {
        this.onSwipe = callback;
    }

    allowSwipe(value) {
        this.targets.forEach((target) => target.interactive = value );
    }
}