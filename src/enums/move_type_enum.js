
export default class MoveTypeEnum {
    constructor() {

    }

    static get MOVE_LEFT() {
        return 'left';
    }

    static get MOVE_RIGHT() {
        return 'right';
    }

    static get MOVE_UP() {
        return 'up';
    }

    static get MOVE_DOWN() {
        return 'down';
    }
}