'use strict';

import * as PIXI from 'pixi.js';
import Cell from "../components/cell";
import SwipeHelper from "../components/swipe_helper";
import TweenMax from "gsap";
import MoveTypeEnum from "../enums/move_type_enum";
import GameModel from "../model/game_model";

export default class GameView extends PIXI.Container {
    constructor(map, gameModel) {
        super();

        this.gameModel = gameModel;

        this.SWAP_TIME_SEC = 0.5 + GameView.DEBUG_TIME;

        this.map = map;

        this.init();
    }

    static get DEBUG_TIME() {
        return 0;
    }

    init() {
        this.field = this.createField();
        this.addChild(this.field);
    }

    createField() {
        this.cells = [];

        let swipeHelper = new SwipeHelper(this);
        swipeHelper.onSwipe(this.onSwipe.bind(this));
        this.swipeHelper = swipeHelper;

        let field = new PIXI.Container();

        for (let i = 0; i < this.gameModel.FIELD_HEIGHT; i++) {
            let row = [];

            for (let j = 0; j < this.gameModel.FIELD_WIDTH; j++) {
                this.createCellBg(this.map[i][j]);

                let cell = new Cell(this.map[i][j].item);
                cell.x = this.map[i][j].x;
                cell.y = this.map[i][j].y;
                cell.onRemoveComplete(this.onCellRemoveComplete.bind(this));
                cell.row = i;
                cell.col = j;

                field.addChild(cell);

                swipeHelper.register(cell);


                row.push(cell);
            }

            this.cells.push(row);
        }

        return field;
    }

    createCellBg(point) {
        let texture = PIXI.utils.TextureCache["cell_bg.png"];
        let bg = new PIXI.Sprite(texture);
        bg.x = point.x;
        bg.y = point.y;
        bg.width = GameModel.CELL_SIZE;
        bg.height = GameModel.CELL_SIZE;
        this.addChild(bg);
    }

    onSwipe(cell, moveType) {
        let neighbourCell;

        switch (moveType) {
            case MoveTypeEnum.MOVE_LEFT:
                if (cell.col - 1 < 0) {
                    return;
                }

                neighbourCell = this.cells[cell.row][cell.col - 1];
                break;

            case MoveTypeEnum.MOVE_RIGHT:
                if (cell.col + 1 >= this.cells[cell.row].length) {
                    return;
                }

                neighbourCell = this.cells[cell.row][cell.col + 1];
                break;

            case MoveTypeEnum.MOVE_UP:
                if (cell.row - 1 < 0) {
                    return;
                }

                neighbourCell = this.cells[cell.row - 1][cell.col];
                break;

            case MoveTypeEnum.MOVE_DOWN:
                if (cell.row + 1 >= this.cells.length) {
                    return;
                }

                neighbourCell = this.cells[cell.row + 1][cell.col];
                break;
        }

        this.swapCells(cell, neighbourCell);

        this.swipeHelper.allowSwipe(false);
    }

    swapCells(cell, neighbourCell) {
        this.cells[cell.row][cell.col] = neighbourCell;
        this.cells[neighbourCell.row][neighbourCell.col] = cell;

        let tempCellData = {
            row: cell.row,
            col: cell.col
        };

        cell.row = neighbourCell.row;
        cell.col = neighbourCell.col;

        neighbourCell.row = tempCellData.row;
        neighbourCell.col = tempCellData.col;

        TweenMax.to(cell, this.SWAP_TIME_SEC, {x: neighbourCell.x, y: neighbourCell.y});
        TweenMax.to(neighbourCell, this.SWAP_TIME_SEC, {x: cell.x, y: cell.y, onComplete: () => this.onUserMove(cell, neighbourCell) });
    }

    revertSwap(cell, neighbourCell) {
        this.cells[cell.row][cell.col] = neighbourCell;
        this.cells[neighbourCell.row][neighbourCell.col] = cell;

        let tempCellData = {
            row: cell.row,
            col: cell.col
        };

        cell.row = neighbourCell.row;
        cell.col = neighbourCell.col;

        neighbourCell.row = tempCellData.row;
        neighbourCell.col = tempCellData.col;

        TweenMax.to(cell, this.SWAP_TIME_SEC, {x: neighbourCell.x, y: neighbourCell.y });
        TweenMax.to(neighbourCell, this.SWAP_TIME_SEC, {x: cell.x, y: cell.y, onComplete: () => this.swipeHelper.allowSwipe(true) });
    }

    onUserMove(callback) {
        this.onUserMove = callback;
    }

    removeItems(items) {
        this.itemsToRemove = items;

        items.forEach(function(item) {
            this.removeItem(item);
        }.bind(this));
    }

    onItemsRemoved(callback) {
        this.onItemsRemoved = callback;
    }

    onCellRemoveComplete(cell, point) {
        this.itemsToRemove.splice(this.itemsToRemove.indexOf(point), 1);

        if (this.itemsToRemove.length === 0) {
            this.onItemsRemoved();
        }
    }

    removeItem(point) {
        let cell = this.cells[point.row][point.col];
        cell.removeItem(point);
    }

    spawnNewItems(emptyPoints) {
        emptyPoints.forEach((point, i, points) => this.spawnNewCell(point, points));
    }

    spawnNewCell(point, points) {
        let tempCell = new Cell(point.item);
        tempCell.x = point.x;
        tempCell.y = point.y - GameModel.SCREEN_HEIGHT * 2;
        this.addChild(tempCell);

        let removeFallingCell = function(point, points, target) {
            this.removeChild(target);

            points.splice(points.indexOf(point), 1);

            let cell = this.cells[point.row][point.col];
            cell.addItem(point.item);

            if (points.length === 0) {
                this.onSpawnNewItemsComplete();
            }
        };

        let delay = 0.1 / point.row;
        if (point.row === 0) {
            delay = 0.1 / 0.99;
        }

        TweenMax.to(tempCell, 0.6 + GameView.DEBUG_TIME, {y: point.y, delay: delay, onCompleteParams: [point, points, tempCell], onComplete: removeFallingCell.bind(this) });
    }

    onSpawnNewItemsComplete(callback) {
        this.onSpawnNewItemsComplete = callback;
    }

    nextTurn() {
        this.swipeHelper.allowSwipe(true);
    }

    fallDown(items) {
        for (let index = 0; index < items.length; index++ ) {
            let item = items[index];
            let cell = this.cells[item.oldRow][item.col];
            let newCell = this.cells[item.row][item.col];

            let tempCell = new Cell(cell.item);
            tempCell.x = cell.x;
            tempCell.y = cell.y;
            this.addChild(tempCell);

            cell.removeItem({}, true);

            let duration = (newCell.y - tempCell.y) * 0.002;

            let tweenCompleteH = function(cell, targets, target) {
                this.removeChild(cell);

                newCell.addItem(cell.item);

                targets.splice(targets.indexOf(target), 1);

                if (targets.length === 0) {
                    this.onFallDownComplete();
                }
            };

            TweenMax.to(tempCell, duration + GameView.DEBUG_TIME, {y: newCell.y, delay: index * 0.05, onCompleteParams: [tempCell, items, item], onComplete: tweenCompleteH.bind(this)});
        }

    }

    onFallDownComplete(callback) {
        this.onFallDownComplete = callback;
    }
}


