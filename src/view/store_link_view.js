'use strict';

import * as PIXI from 'pixi.js';
import OSHelper from "../utils/os_helper";
import GameModel from "../model/game_model";

export default class StoreLinkView extends PIXI.Container {
    constructor() {
        super();

        this.init();
    }

    init() {
        this.element = document.createElement('a');
        this.element.target = '_blank';

        let osHelper = new OSHelper();

        let label = '';

        switch (osHelper.os) {
            case 'Android':
                label = 'Tap to open Google Play Market';
                this.element.href = GameModel.URL_GOOGLE_PLAY_MARKET;
                break;

            case 'iOS':
                label = 'Tap to open Apple Store';
                this.element.href = GameModel.URL_APPLE_STORE;
                break;

            default:
                label = 'Please, open on mobile to see the link to the Store';
                this.element.href = GameModel.URL_GOOGLE;

        }



        const style = new PIXI.TextStyle({
            fontFamily: 'Arial',
            fontSize: 20,
            fontWeight: 'bold',
            fill: ['#ffffff'],
            dropShadow: true,
            dropShadowColor: '#000000',
            dropShadowBlur: 2,
            dropShadowDistance: 2,
            strokeThickness: 2,
        });

        let linkText = new PIXI.Text(label, style);
        linkText.anchor.x = 0.5;
        linkText.x = GameModel.SCREEN_WIDTH / 2;
        linkText.y = GameModel.SCREEN_HEIGHT - linkText.height - 10;
        linkText.interactive = true;
        linkText.on('pointerdown', this.onTextClick.bind(this));

        this.addChild(linkText);
    }

    onTextClick() {
        let e = window.document.createEvent("MouseEvents");
        e.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        this.element.dispatchEvent(e);
    }
}