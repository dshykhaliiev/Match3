'use strict';

import GameModel from "../model/game_model";
import GameView from "../view/game_view";
import MapHelper from "../components/map_helper";
import StoreLinkView from "../view/store_link_view";
import JSONLoader from "../utils/json_loader";
import Tutorial from "../components/tutorial";


export default class GameController {
    constructor(app, resizer) {
        this.app = app;
        this.resizer = resizer;

        PIXI.loader.add('assets/sprites.json')
            .load(this.init.bind(this));
    }

    init() {
        let promise = new Promise(this.loadConfig);
        promise.then(this.initModels.bind(this));
        promise.then(this.generateMap.bind(this));
        promise.then(this.startGame.bind(this));
    }

    loadConfig(resolve, reject) {
        let jsonLoader = new JSONLoader();
        jsonLoader.onLoaded(function(configObj) {
            resolve(configObj);
        });
        jsonLoader.load(GameModel.CONFIG_URL);
    }


    initModels(configObj) {
        this.model = new GameModel(configObj);
    }

    generateMap() {
        this.mapHelper = new MapHelper(this.model);
        this.model.map = this.mapHelper.generate();
    }

    startGame() {
        const gameView = new GameView(this.model.map, this.model);
        gameView.onUserMove(this.onUserMove.bind(this));
        gameView.onItemsRemoved(this.onItemsRemoved.bind(this));
        gameView.onSpawnNewItemsComplete(this.onSpawnNewItemsComplete.bind(this));
        gameView.onFallDownComplete(this.onFallDownComplete.bind(this));

        this.app.stage.addChild(gameView);

        this.gameView = gameView;

        this.showTutorial();
    }

    showTutorial() {
        this.resizer.enableTutorial();

        this.tutorial = new Tutorial(this.model.map);
        this.app.stage.addChild(this.tutorial);
    }

    onUserMove(cell, neighbourCell) {
        let point = this.model.map[neighbourCell.row][neighbourCell.col];
        let neighbourPoint = this.model.map[cell.row][cell.col];

        let tempItem = point.item;
        point.item = neighbourPoint.item;
        neighbourPoint.item = tempItem;

        let itemsToRemove = this.mapHelper.getItemsToRemove(point, neighbourPoint, this.model.map);

        if (itemsToRemove.length > 0) {
            this.removeTutorial();

            itemsToRemove.forEach( item => item.item = -1 );

            this.gameView.removeItems(itemsToRemove);
        } else {
            let tempItem = point.item;
            point.item = neighbourPoint.item;
            neighbourPoint.item = tempItem;

            this.gameView.revertSwap(cell, neighbourCell);
        }
    }

    removeTutorial() {
        if (!this.tutorial) {
            return;
        }

        this.resizer.disableTutorial();
        this.resizer.needShowTut = false;

        this.app.stage.removeChild(this.tutorial);
        this.tutorial = null;

        this.showStoreLinkView();
    }

    onItemsRemoved() {
        let fallItems = this.mapHelper.fallDown(this.model.map);

        if (fallItems.length > 0) {
            this.fallDown(fallItems);
        } else {
            this.onFallDownComplete();
        }
    }

    fallDown(fallItems) {
        this.gameView.fallDown(fallItems);
    }

    onFallDownComplete() {
        let itemsToRemove = this.mapHelper.getAllItemsToRemove(this.model.map);

        if (itemsToRemove.length > 0) {
            itemsToRemove.forEach(item => item.item = -1);

            this.gameView.removeItems(itemsToRemove);
        } else {
            this.fillMap();
        }
    }

    fillMap() {
        let emptyPoints = this.mapHelper.fillEmptyPoints(this.model.map);

        this.gameView.spawnNewItems(emptyPoints);
    }

    onSpawnNewItemsComplete() {
        let itemsToRemove = this.mapHelper.getAllItemsToRemove(this.model.map);

        if (itemsToRemove.length > 0) {
            this.gameView.removeItems(itemsToRemove);

            itemsToRemove.forEach(item => item.item = -1);
        } else {
            this.gameView.nextTurn();
        }
    }

    showStoreLinkView() {
        const linkView = new StoreLinkView();
        this.app.stage.addChild(linkView);
    }
}