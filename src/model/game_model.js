'use strict';

export default class GameModel {
    constructor(configObj) {
        this.map = [];

        this.fieldWidth = Math.min(configObj.fieldWidth, 6);
        this.fieldHeight = Math.min(configObj.fieldHeight, 6);

        this.fieldWidth = Math.max(this.fieldWidth, 3);
        this.fieldHeight = Math.max(this.fieldHeight, 3);
    }

    static get SCREEN_WIDTH() {
        return 500;
    }

    static get SCREEN_HEIGHT() {
        return 500;
    }

    static get ASPECT_RATIO() {
        return this.SCREEN_WIDTH / this.SCREEN_HEIGHT;
    }

    static get FIELD_TOP_PADDING() {
        return 10;
    }

    static get FIELD_LEFT_PADDING() {
        return 10;
    }


    get FIELD_WIDTH() {
        return this.fieldWidth;
    }

    get FIELD_HEIGHT() {
        return this.fieldHeight; // 6
    }

    static get CELL_SIZE() {
        return 80;
    }

    static get URL_APPLE_STORE() {
        return 'https://itunes.apple.com/ua/app/minecraft/id479516143?l=ru&mt=8'
    }

    static get URL_GOOGLE_PLAY_MARKET() {
        return 'https://play.google.com/store/apps';
    }

    static get URL_GOOGLE() {
        return 'https://google.com'
    }

    static get CONFIG_URL() {
        return 'config.json';
    }
}