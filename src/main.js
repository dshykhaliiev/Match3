'use strict';

import * as PIXI from 'pixi.js';
import GameController from "./controller/game_controller";
import GameModel from "./model/game_model";
import Resizer from "./utils/resizer";

let app = new PIXI.Application(GameModel.SCREEN_WIDTH, GameModel.SCREEN_HEIGHT, {transparent: true});

document.body.appendChild(app.view);

let resizer = new Resizer(app.view);

let gameController = new GameController(app, resizer);

